# Penggunaan GIT pada sebuah Project

## Pendahuluan

GIT merupakan salah satu sistem untuk mengotrol versi dari project yang dibangun. GIT berfungsi untuk mencatat setiap perubahan pada file project yang dikerjakan baik sendiri maupun banyak orang. Jika ingin mengetahui lebih lanjut, silahkan klik [link berikut](https://git-scm.com/).

Pada program magang di PT.Javan Cipta Solusi, disana diajarkan bagaimana cara untuk menggunakan GIT dalam project kita. Berikut merupakan beberapa fitur utama yang dapat dipelajari terlebih dahulu.

### Cloning Repository

Pertama adalah Cloning Repository - Disini, kita belajar untuk dapat menduplikasi data pada cloud ke local kita masing-masing. Caranya adalah terlebih dahulu pastikan kalian sudah menginstall [GIT] (https://git-scm.com/) pada perangkat kalian.

Lalu, buka directory local yang ingin kalian isi dengan folder project kalian di gitlab ataupun github. Setelah itu, buka repository kalian dan pilih "clone" dengan format https dan copy link yang ada.

Buka GIT Bash pada directory local kalian. Selanjutnya masukkan code berikut :

```
git clone (URL yang sudah di copy)
```

Jika sukses, maka folder project anda akan ada di directory local kalian.

### Branch, Commit dan Push

Branch merupakan fitur untuk membantu developer dalam mengatur beberapa versi dari aplikasi yang dikembangkan. berikut beberapa fungsi utama dari fitur branch adalah memungkinkan pengembangan fitur baru bagi aplikasi tanpa menganggu pengembangan di branch utama dan memungkinkan pembuatan branch-branch pengembang berbeda yang dapat berpusat di satu repositori. Contohnya stable branch, test branch dan unstable branch.

Jika kita melakukan kolaborasi dengan orang lain, kita harus menggunakan branch yang berbeda atau tidak di branch master karena untuk mencegah terjadinya konflik antara code developer satu dengan yang lainnya

Cara membuat branch baru adalah dengan code berikut ini

```
git branch (nama branch yang ingin dibuat)
```

Untuk berpindah dari branch satu ke branch lainnya adalah menggunakan code berikut ini

```
git checkout (nama branch tujuan)
```

Selanjutnya adalah Commit dan Push, Commit berfungsi untuk menyimpan perubahan yang dilakukan. Push berfungsi untuk mengirimkan perubahan file setelah di commit ke remote repository.

Berikut langkah-langkah untuk melakukan Commit. Pertama, pastikan kalian sudah memberikan pembaharuan pada direktori local kalian. Contohnya seperti perubahan code, penambahan file dan lain-lainnya. Selanjutnya, masukkan kode berikut untuk mengawali proses commit ini

```
git add .
```

Kemudian, kalian tuliskan pesan singkat terkait dengan commit kalian ini. Contohnya seperti "update fitur login". Kodenya adalah sebagai berikut

```
git commit -m (pesan kalian)
```

Setelah melakukan ```add``` dan ```commit``` selanjutnya adalah untuk ```push```. Push berfungsi untuk mengunggah pembaharuan yang telah di ```add``` dan ```commit``` ke dalam repository cloud kalian. Cara push adalah dengan kode berikut

```
git push origin (nama branch tujuan)
```
